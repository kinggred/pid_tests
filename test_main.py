from click.testing import CliRunner
from main import check, generate
from generate_main import *

generator = Generator("man", 22, 3, 2005)
generator.month_pid()
generator.gender_generator()
finish = generator.control_generator()
runner = CliRunner()


def test_click_check():
    result = runner.invoke(check, ["05232205612"])
    assert " Control number should be: 2" in result.output
    assert " Your PID have an correct control number" in result.output
    assert " Your gender is: Man" in result.output
    assert " You've been born in:  22 . 3 . 2005" in result.output
    assert " Your age is:  16" in result.output


def test_click_generate():
    result = runner.invoke(generate, ["man", "22", "3", "2005"])
    assert "Your generated PID is: " in result.output
    assert "052322" in result.output[23:]


def test_pid_lenght():
    assert len(generator.control_generator()) == 11


def test_gender():
    assert generator.gender == "man"


def test_year():
    assert finish[0:2] == "05"


def test_month():
    assert finish[2:4] == "23"


def test_day():
    assert finish[4:6] == "22"


def test_control():
    pid = "05232205612"
    assert pid[10:11] == "2"
