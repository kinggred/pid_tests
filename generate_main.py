import random


class Generator:
    def __init__(self, gender, day, month, year):
        self.gender = gender
        self.day = day
        self.month = month
        self.year = year

    def month_pid(self):
        global c_month

        if 0 >= self.day or self.day > 31:
            raise ValueError("WRONG DAY INPUT!")
        if 0 >= self.month or self.month > 12:
            raise ValueError(" WRONG MONTH INPUT!")

        if 1800 <= self.year <= 1899:
            c_month = self.month + 80
        elif 1900 <= self.year <= 1999:
            c_month = self.month
        elif 2000 <= self.year <= 2099:
            c_month = self.month + 20
        elif 2100 <= self.year <= 2199:
            c_month = self.month + 40
        elif 2200 <= self.year <= 2299:
            c_month = self.month + 60
        else:
            raise ValueError("WRONG YEAR INPUT!")
        return c_month

    def gender_generator(self):
        global generate_random_number, rolled_number_of_gender
        generate_random_number = random.randint(0, 999)
        man = [1, 3, 5, 7, 9]
        woman = [0, 2, 4, 6, 8]
        gender_number_randomize = random.randint(0, 4)

        if self.gender == "man":
            rolled_number_of_gender = man[gender_number_randomize]
        elif self.gender == "woman":
            rolled_number_of_gender = woman[gender_number_randomize]
        else:
            raise ValueError("WRONG GENDER TYPE!")

    def control_generator(self):
        pid_year = self.year % 100
        gender_output = str(
            "%03d%01d" % (generate_random_number, rolled_number_of_gender)
        )
        outcome: str = str(
            "%02d%02d%02d%s" % (pid_year, c_month, self.day, gender_output)
        )

        control_number = 0
        index = 0
        for number in outcome:
            if index == 0 or index == 4 or index == 8:
                control_number += (int(number)) * 1 % 10
            if index == 1 or index == 5 or index == 9:
                control_number += (int(number)) * 3 % 10
            if index == 2 or index == 6:
                control_number += (int(number)) * 7 % 10
            if index == 3 or index == 7:
                control_number += (int(number)) * 9 % 10
            index += 1
        control_number = control_number % 10
        control_number = 10 - int(control_number)
        if control_number == 10:
            control_number = 0
        finish = "%s%01d" % (outcome, control_number)
        return finish
