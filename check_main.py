from datetime import date


class Checker:
    def __init__(
        self,
        gender_number,
        birth_day,
        birth_month,
        birth_year,
        control_last,
        personal_id,
    ):
        self.personal_id = personal_id
        self.gender_number = gender_number
        self.birth_day = birth_day
        self.birth_month = birth_month
        self.birth_year = birth_year
        self.control_last = control_last

    def how_long_is_pid(self):
        if len(self.personal_id) != 11:
            raise ValueError("Too short or too long PID number")

    def century_enumeration(self):
        global birth_month, century
        if 1 <= int(self.birth_month) <= 12:
            century = "19"
        elif 21 <= int(self.birth_month) <= 32:
            century = "20"
            birth_month = int(self.birth_month) - 20
        elif 41 <= int(self.birth_month) <= 52:
            century = "30"
            birth_month = int(self.birth_month) - 40
        elif 61 <= int(self.birth_month) <= 72:
            century = "40"
            birth_month = int(self.birth_month) - 60
        elif 81 <= int(self.birth_month) <= 92:
            century = "18"
            birth_month = int(self.birth_month) - 80
        else:
            raise ValueError("Wrong PID number")
        return century, birth_month

    def whats_the_day(self):
        if int(self.birth_day) > 31 or int(self.birth_day) < 0:
            raise ValueError("Wrong PID number")

    def whats_the_gender(self):
        if int(self.gender_number) % 2 == 0:
            return "Woman"
        return "Man"

    def whats_the_control_number(self):
        control_number = 0
        index = 0

        for number in self.personal_id:
            if index == 0 or index == 4 or index == 8:
                control_number += (int(number)) * 1 % 10
            if index == 1 or index == 5 or index == 9:
                control_number += (int(number)) * 3 % 10
            if index == 2 or index == 6:
                control_number += (int(number)) * 7 % 10
            if index == 3 or index == 7:
                control_number += (int(number)) * 9 % 10
            index += 1
        control_number = control_number % 10
        control_number = 10 - control_number
        if control_number == 10:
            control_number = 0
        return control_number

    def date_right_now(self):
        real_year = int("%s%s" % (century, self.birth_year))
        date_now = date.today()
        year_now = date_now.year
        month_now = date_now.month
        day_now = date_now.day
        if int(month_now) < int(self.birth_month):
            age = year_now - int(real_year)
        else:
            if int(day_now) < int(self.birth_day):
                age = year_now - int(real_year)
            else:
                age = year_now - int(real_year) - 1
        return age
