class Months:
    def __init__(self, month, year, day):
        self.month = month
        self.year = year
        self.day = day

    def february(self):
        if self.month == 2:
            if (self.year % 100 != 0 or self.year % 400 == 0) and self.year % 4 == 0:
                if self.day > 29 or self.day < 0:
                    raise ValueError(" THIS MONTH HAVE ONLY 29 DAYS!")
            else:
                if self.day < 0 or self.day > 28:
                    raise ValueError(" THIS MONTH HAVE ONLY 28 DAYS!")

        if self.month in [4, 6, 9, 11]:
            if self.day > 30 or self.day < 0:
                raise ValueError(" THIS MONTH HAVE ONLY 30 DAYS!")
