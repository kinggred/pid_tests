import click
from check_main import *
from generate_main import *
from lengh_of_months import *


@click.group()
def cli():
    pass


@cli.command()
@click.argument("personal_id", type=str)
def check(personal_id):
    gender_number = personal_id[6:10]
    birth_day = personal_id[4:6]
    birth_month = personal_id[2:4]
    birth_year = personal_id[0:2]
    control_last = personal_id[10:11]

    lang = Months(birth_month, birth_year, birth_day)
    c = Checker(
        gender_number, birth_day, birth_month, birth_year, control_last, personal_id
    )

    lang.february()
    c.how_long_is_pid()
    century, birth_month = c.century_enumeration()
    c.whats_the_day()
    gender = c.whats_the_gender()
    control_number = c.whats_the_control_number()
    age = c.date_right_now()

    print("\n Control number should be:", control_number)
    if int(control_last) == int(control_number):
        print(" Your PID have an correct control number")
    else:
        raise ValueError(" Your PID have an incorrect control number")
    print(" Your gender is:", gender)
    print(
        " You've been born in: ",
        birth_day,
        ".",
        birth_month,
        ". %s%s" % (century, birth_year),
    )
    print(" Your age is: ", age)


@cli.command()
@click.argument("gender", type=str)
@click.argument("day", type=int)
@click.argument("month", type=int)
@click.argument("year", type=int)
def generate(gender, day, month, year):
    g = Generator(gender, day, month, year)
    length = Months(month, year, day)

    g.month_pid()
    length.february()
    g.gender_generator()
    finish = g.control_generator()

    print("Your generated PID is: %s" % finish)


if __name__ == "__main__":
    cli()
